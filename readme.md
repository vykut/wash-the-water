# Wash The Water 
<h5>by Tacky Ozone</h5>

* [App Statement](#statement)
* [Introduction](#intro)
* [Journey](#journey)
* [Team Members](#team-members)

# <a name="statement"></a>App Statement
An App that ... and that ... in order to ... by ...           

# <a name="intro"></a>Introduction
The challenge that we wanted to address is ...

# <a name="journey"></a>Journey
<b>Tacky Ozone</b> first engaged in identifying a compelling challenge, then we started an investigation on ... that led us to the solution. 

# <a name="team-members"></a>Team Members
* "Luigi Barretta" <email@outlook.com>
* "Julia Tsoi" <email@gmail.com>
* "Victor Socaciu" <email@gmail.com>
