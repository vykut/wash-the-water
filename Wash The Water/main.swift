//
//  main.swift
//  WashTheWater
//
//  Created by Luigi Barretta on 06/01/2019.
//  Copyright © 2019 AcademyProjects. All rights reserved.
//

import Foundation
import UIKit

/*
 The app event cycle = TimerApplication -> AppDelegate
 */

UIApplicationMain(
  CommandLine.argc,
  CommandLine.unsafeArgv,
  NSStringFromClass(TimerApplication.self),
  NSStringFromClass(AppDelegate.self)
)

