//
//  ViewController.swift
//  Wash The Water
//
//  Created by Victor Socaciu on 10/12/2018.
//  Copyright © 2018 AcademyProjects. All rights reserved.
//

import UIKit
import AVFoundation

class MainViewController: UIViewController {
  
  @IBOutlet weak var italianSubtitle: UILabel!
  @IBOutlet weak var StackView: UIStackView!
  @IBOutlet weak var StartButton: UIButton!
  
  @IBOutlet weak var washTitle: UILabel!
  

  
  override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.



    }
  
  override func viewWillAppear(_ animated: Bool) {
    //This shows the navigation controller on the top border
    navigationController?.setNavigationBarHidden(true, animated: false)
    HapticFeedback.selection.prepare()
    GlobalVariable.SCREEN_UNIT = GlobalVariable.SCREEN_UNIT * UIScreen.main.bounds.width
    UserDefaults.standard.set(true, forKey: GlobalVariable.FIRST_LAUNCH)
    
    if GlobalVariable.GENERIC_SWITCH {
      italianSubtitle.isHidden = false
    } else {
      italianSubtitle.isHidden = true

    }
    


  }
  
  
  @IBAction func startButtonTouched(_ sender: Any) {
   print("StartButton tapped")
    HapticFeedback.selection.selectionChanged()
    

  }
  
  @objc func buttonAction(sender: UIButton!) {
    print("Button tapped")
  }
  


}

extension UIApplication {
  class func topViewController(base: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
    if let nav = base as? UINavigationController {
      return topViewController(base: nav.visibleViewController)
    }
    if let tab = base as? UITabBarController {
      if let selected = tab.selectedViewController {
        return topViewController(base: selected)
      }
    }
    if let presented = base?.presentedViewController {
      return topViewController(base: presented)
    }
    return base
  }
}
