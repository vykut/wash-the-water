//
//  storyPage1.swift
//  WashTheWater
//
//  Created by Luigi Barretta on 23/12/2018.
//  Copyright © 2018 AcademyProjects. All rights reserved.
//

import UIKit
import Foundation

class StoryPage1: UIViewController {
  
  @IBOutlet weak var skipButton: UIButton!
  var myButton = UIButton()
  var storyOverlayUI = PageViewUI()
  
  override func viewDidLoad() {
    super.viewDidLoad()
    // Do any additional setup after loading the view, typically from a nib.
    
    
  }
  
  override func viewWillAppear(_ animated: Bool) {
    
    skipButton.setTitleColor(.black, for: UIControl.State.highlighted)
    skipButton.setTitleColor(.black, for: UIControl.State.selected)
    skipButton.setTitleShadowColor(UIColor.black, for: UIControl.State.normal)
    
  }
  
  
  
  
}
