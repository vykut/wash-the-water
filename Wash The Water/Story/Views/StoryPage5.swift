//
//  storyPage5.swift
//  WashTheWater
//
//  Created by Luigi Barretta on 23/12/2018.
//  Copyright © 2018 AcademyProjects. All rights reserved.
//

import UIKit

class StoryPage5: UIViewController {
  
  @IBOutlet weak var lightningHero: UIButton!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    // Do any additional setup after loading the view, typically from a nib.
    
    
    
    
  }
  
  override func viewWillAppear(_ animated: Bool) {
    
    //Animate the Hero: pulse
    UIView.animate(withDuration: 2, delay: 0.0, options: [.repeat, .autoreverse, UIView.AnimationOptions.allowUserInteraction, UIView.AnimationOptions.curveLinear], animations: {
      
      self.lightningHero.transform = CGAffineTransform(scaleX: 0.5, y: 0.5)
      
    }, completion: { (finished: Bool) in
      //      self.lightningHero.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
    })
    
  }
  
  
}
