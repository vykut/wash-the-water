//
//  PageViewController.swift
//  Story of the Challenge
//
//  Created by Marco Giannattasio on 12/12/2018.
//  Copyright © 2018 Marco Giannattasio. All rights reserved.
//

import UIKit
import SpriteKit
import AVFoundation

class PageViewController: UIPageViewController, UIPageViewControllerDelegate, UIPageViewControllerDataSource {
  
  //  var pageViewUI = PageViewUI()
  var numberOfSwipes = 0
  let pageControl = UIPageControl()
  
  //Associate view controller
  lazy var orderedViewControllers: [UIViewController] = {
    return [self.newVc(viewController: "StoryA"),
            self.newVc(viewController: "StoryB"),
            self.newVc(viewController: "StoryC"),
            self.newVc(viewController: "StoryD"),
            self.newVc(viewController: "StoryE")]
  }()
  
  //First function called
  override func viewDidLoad() {
    super.viewDidLoad()
    
    
    //This lets us edit view properties
    self.pageControl.translatesAutoresizingMaskIntoConstraints = false
    
    self.scrollView?.panGestureRecognizer.addTarget(self, action: #selector(self.handlePan(sender:)))
    
    //View properties
    self.pageControl.frame = CGRect()
    self.pageControl.center.x = view.center.x
    self.pageControl.center.y = view.frame.maxY - 50
    self.pageControl.currentPageIndicatorTintColor = UIColor.black
    self.pageControl.pageIndicatorTintColor = UIColor.lightGray
    self.pageControl.numberOfPages = 5
    self.pageControl.currentPage = 0
    
    pageControl.translatesAutoresizingMaskIntoConstraints = true
    pageControl.autoresizingMask = [UIView.AutoresizingMask.flexibleLeftMargin, UIView.AutoresizingMask.flexibleRightMargin, UIView.AutoresizingMask.flexibleTopMargin, UIView.AutoresizingMask.flexibleBottomMargin]
    
    self.pageControl.pageIndicatorTintColor = .white
    
    view.addSubview(pageControl)
    
    
    //    pageViewUI.setSkipStoryButton()
    //    view.addSubview(pageViewUI.skipStoryButton)
    //    pageViewUI.skipStoryButton.addTarget(self, action: #selector(pressedAction(_:)), for: .touchUpInside)
    print(">.PageViewController max page number: \(orderedViewControllers.count)")
    
    
    self.dataSource = self
    if let firstViewController = orderedViewControllers.first {
      setViewControllers([firstViewController], direction: .forward, animated: true, completion: nil)
      
      
    }
    
  }
  
  override func viewWillAppear(_ animated: Bool) {
    //This shows the navigation controller on the top border
    navigationController?.setNavigationBarHidden(false, animated: false)
    HapticFeedback.notification.generator.prepare()
    
    
    
  }
  
  @objc func pressedAction(_ sender: UIButton) {
    // do your stuff here
    print("you clicked on button \(sender.accessibilityIdentifier ?? "unknownBtn")")
    
    HapticFeedback.notification.success
    
    //    AudioServicesPlayAlertSound(kSystemSoundID_Vibrate)
    
    //    present(pageViewUI.gameViewController, animated: true, completion: nil)
    
  }
  
  func newVc(viewController: String) -> UIViewController {
    
    //Returns the Storyboard in which the pages are built, in this case Story.storyboard
    return UIStoryboard(name: "Story", bundle: nil).instantiateViewController(withIdentifier: viewController)
    
  }
  
  //Page view movement towards left
  func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
    guard let viewControllerIndex = orderedViewControllers.index(of: viewController) else {
      return nil
    }
    
    
    pageControl.currentPage = viewControllerIndex
    
    if viewControllerIndex >= 1 {
      //      pageViewUI.skipStoryButton.setTitle("Skip", for: .normal)
      //      pageViewUI.skipStoryButton.setImage(pageViewUI.skipStoryIcon, for: UIControl.State.normal)
      //      pageViewUI.resetSkipButtonBackgroundColor()
      
      return orderedViewControllers[viewControllerIndex - 1]
    } else if viewControllerIndex == orderedViewControllers.count - 1 {
      //      pageViewUI.skipStoryButton.setImage(pageViewUI.startGameIcon, for: UIControl.State.normal)
      
      
      return nil
      
    } else { return nil }
    
  }
  
  //Page view movement towards right
  func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
    guard let viewControllerIndex = orderedViewControllers.index(of: viewController) else {
      return nil
    }
    
    pageControl.currentPage = viewControllerIndex
    
    //When it shows till to second-last page
    if viewControllerIndex < orderedViewControllers.count - 1 {
      
      //      pageViewUI.skipStoryButton.setTitle("Skip", for: .normal)
      //      pageViewUI.skipStoryButton.setImage(pageViewUI.skipStoryIcon, for: UIControl.State.normal)
      //      pageViewUI.resetSkipButtonBackgroundColor()
      
      return orderedViewControllers[viewControllerIndex + 1]
      
    }
      //When it shows the last page
    else if viewControllerIndex == orderedViewControllers.count - 1 {
      //      pageViewUI.skipStoryButton.backgroundColor = pageViewUI.skipStartButtonColors[1]
      //      pageViewUI.skipStoryButton.setImage(pageViewUI.startGameIcon, for: UIControl.State.normal)
      
      
      //      pageViewUI.skipStoryButton.setTitle("Start", for: .normal)
      return nil
      
    }
      //When it shows after last page - NOTHING
    else { return nil }
  }
  
  @objc func handlePan(sender:UIPanGestureRecognizer) {
    switch sender.state {
    case .ended:
      self.numberOfSwipes = self.numberOfSwipes + 1
      print("User Swiped ")
      print(self.numberOfSwipes)
    default:
      break
    }
  }
  
  
}
