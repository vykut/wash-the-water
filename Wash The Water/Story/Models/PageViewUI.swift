//
//  StoryPages.swift
//  WashTheWater
//
//  Created by Luigi Barretta on 23/12/2018.
//  Copyright © 2018 AcademyProjects. All rights reserved.
//

import UIKit

//This class contains all the UI elements programmatically created for the Story>PageViewController
class PageViewUI: UIViewController {
  
  var skipStoryButton = UIButton()
  var skipStoryIcon = UIImage(named: "SkipButton")
  var startGameIcon = UIImage(named: "StartButton")
  var startGameIconSelected = UIImage(named: "StartButton-Selected")
  var skipStartButtonColors = [#colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)]
  
  var storyBoard = UIStoryboard(name: "Game", bundle: nil)
  var gameViewController = GameViewController()
  
  func setSkipStoryButton() {
    
    //    skipStoryButton.setTitle("Skip", for: .normal)
    //    skipStoryButton.backgroundColor = UIColor.init(hex: 0x4EB4CC)
    
    skipStoryButton.setImage(skipStoryIcon, for: UIControl.State.normal)
    skipStoryButton.setImage(startGameIconSelected, for: UIControl.State.highlighted)
    skipStoryButton.setImage(startGameIconSelected, for: UIControl.State.selected)
    
    skipStoryButton.backgroundColor = skipStartButtonColors[0]
    
    //    skipStoryButton.setTitleColor(UIColor.white, for: .normal)
    skipStoryButton.layer.cornerRadius = 35
    skipStoryButton.layer.borderWidth = 1
    skipStoryButton.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
    skipStoryButton.alpha = 0.8
    
    skipStoryButton.frame = CGRect(x: UIScreen.main.bounds.maxX - RIGHTSAFEAREA - 100 , y: UIScreen.main.bounds.minY + TOPSAFEAREA + 20, width: 70, height: 70)
    
    
    skipStoryButton.imageView?.contentMode = .scaleAspectFit
    
    
    
    skipStoryButton.tag = 11
    skipStoryButton.accessibilityIdentifier = "SkipBtn"
    
    gameViewController = storyBoard.instantiateViewController(withIdentifier: "Game") as! GameViewController
  }
  
  func resetSkipButtonBackgroundColor() {
    skipStoryButton.backgroundColor = .white
    
  }
  
  func hideSkipStoryButton() {
    skipStoryButton.isHidden = true
  }
  
  func showSkipStoryButton() {
    skipStoryButton.isHidden = false
  }
  
  
}

extension UIPageViewController {
  
  public var scrollView: UIScrollView? {
    for view in self.view.subviews {
      if let scrollView = view as? UIScrollView {
        return scrollView
      }
    }
    return nil
  }
  
}
