//
//  AppDelegate.swift
//  Wash The Water
//
//  Created by Victor Socaciu on 10/12/2018.
//  Copyright © 2018 AcademyProjects. All rights reserved.
//

import UIKit
import AVFoundation

//TimerApplication replaces AppDelegate with some custom code, see main.swift
//@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    
    var audioPlayer: AVAudioPlayer!
    var soundTrack: AVAudioPlayer!
    var screenOrientationState: String!
  
  
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
      

        self.playSoundTrack()
      

      NotificationCenter.default.post(name: .appOrientation, object: nil)
      
        application.isIdleTimerDisabled = false
      //Use the following line to invoke it on another view
      //UIApplication.shared.isIdleTimerDisabled = true
      

      
        return true
    }
  
  func playSoundTrack(){
    print(">.playSoundTrack")
    let path = Bundle.main.path(forResource: Constants.SoundFile.BackgroundMusic, ofType:"m4a")!
    let url = URL(fileURLWithPath: path)
    
    let isMuted = UserDefaults.standard.bool(forKey: Constants.MusicState.MUSIC_MUTED)
    
    
    print(">.soundTrackURL: \(url)")
    
    do {
      soundTrack = try AVAudioPlayer(contentsOf: url)
      
    } catch {
      // couldn't load file :(
      print(error.localizedDescription)
      
    }
    
    soundTrack.volume = 0.1
    soundTrack.numberOfLoops = -1
    
    if !isMuted {
      soundTrack.play()
    } else {
        soundTrack.stop()

    }

  }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
      print(">.applicationWillResignActive")
      
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
      print(">.applicationDidEnterBackground")

    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
      print(">.applicationWillEnterForeground")


    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
      print(">.applicationDidBecomeActive")

    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
      print(">.applicationWillTerminate")

    }
  
  
  

}

