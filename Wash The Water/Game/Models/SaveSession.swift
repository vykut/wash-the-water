//
//  SaveSession.swift
//  WashTheWater
//
//  Created by Luigi Barretta on 22/12/2018.
//  Copyright © 2018 AcademyProjects. All rights reserved.
//

import UIKit
import CoreGraphics

struct SaveSession : Codable {
  var id: Int
  var levelIdentifier: Int
  var finalScore: CGFloat
  var remainingLives: Int
}
