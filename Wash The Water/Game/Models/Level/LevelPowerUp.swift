//
//  PowerUp.swift
//  WashTheWater
//
//  Created by Luigi Barretta on 22/12/2018.
//  Copyright © 2018 AcademyProjects. All rights reserved.
//

import Foundation
import CoreGraphics

struct LevelPowerUp : Codable {
  var type : PowerUpType
}


enum PowerUpType: String, Codable {
  case resizing
  case other
  case generic
  case unknown
  
}

extension PowerUpType {
  public init(from decoder: Decoder) throws {
    self = try PowerUpType(rawValue: decoder.singleValueContainer().decode(String.self)) ?? .unknown
  }
}


