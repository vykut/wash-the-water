//
//  Level.swift
//  WashTheWater
//
//  Created by Luigi Barretta on 20/12/2018.
//  Copyright © 2018 AcademyProjects. All rights reserved.
//

import Foundation
import CoreGraphics

struct Level : LevelScene, Codable {
  
//---Extended from LevelScene----------
  var levelId: Int
  var title: String
  var description: String
  var lastScore: Int
  var bestScore: Int
  var difficulty: String
  var accomplished: Bool
//-------------------------------------
  
  var player: LevelPlayer
  var enemy: [LevelEnemy]
  var powerUp: [LevelPowerUp] 
  var numberOfEnemies: Int
  var numberOfCircles: Int
  var gameSpeedMultiplier: Int
  
}

extension Level: Equatable {
  static func ==(lhs: Level, rhs: Level) -> Bool {
    return lhs.title == rhs.title
  }
}



