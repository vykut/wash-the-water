//
//  Hero.swift
//  WashTheWater
//
//  Created by Luigi Barretta on 22/12/2018.
//  Copyright © 2018 AcademyProjects. All rights reserved.
//

import Foundation
import CoreGraphics

struct LevelPlayer : Codable {
  var type: String
  var id: Int
  var sizeMultiplier: CGFloat
  var numberOfLives: Int
  var position: CGPoint
}
