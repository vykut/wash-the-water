//
//  BadGuy.swift
//  WashTheWater
//
//  Created by Luigi Barretta on 22/12/2018.
//  Copyright © 2018 AcademyProjects. All rights reserved.
//

import Foundation
import CoreGraphics

struct LevelEnemy : Codable {
  var type: EnemyType
  var id: Int
  var sizeMultiplier: CGFloat
  
}

enum EnemyType: String, Codable {
  case badGuy
  case other
  case unknown
  
}

extension EnemyType {
  public init(from decoder: Decoder) throws {
    self = try EnemyType(rawValue: decoder.singleValueContainer().decode(String.self)) ?? .unknown
  }
}
