//
//  Scene.swift
//  WashTheWater
//
//  Created by Luigi Barretta on 20/12/2018.
//  Copyright © 2018 AcademyProjects. All rights reserved.
//

import Foundation
import CoreGraphics

protocol LevelScene {
  var levelId: Int { get set }
  var title: String { get set }
  var description: String { get set }
  var lastScore: Int { get set }
  var bestScore: Int { get set }
  var accomplished: Bool { get set }
  var difficulty: String { get set }
  
}
