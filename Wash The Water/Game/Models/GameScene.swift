//
//  GameScene.swift
//  Wash The Water
//
//  Created by Victor Socaciu on 11/12/2018.
//  Copyright © 2018 AcademyProjects. All rights reserved.
//

import UIKit
import SpriteKit
import CoreMotion
import AudioToolbox

//Index used to move across levels
var indexLevel : Int = 0
var saveSessionIndex : Int = 0

var TOPSAFEAREA : CGFloat = UIApplication.shared.keyWindow?.safeAreaInsets.top ?? 44.0
var BOTTOMSAFEAREA : CGFloat = UIApplication.shared.keyWindow?.safeAreaInsets.bottom ?? 34.0
var LEFTSAFEAREA : CGFloat = UIApplication.shared.keyWindow?.safeAreaInsets.left ?? 0.0
var RIGHTSAFEAREA : CGFloat = UIApplication.shared.keyWindow?.safeAreaInsets.right ?? 0.0

class GameScene: SKScene, SKPhysicsContactDelegate {
  
  var motionManager: CMMotionManager?
  var saveSessions : [SaveSession] = []
  
  // MARK: - Declare scene elements: characters and foreground
  //Characters
  var player: Hero
  var enemies: SKNode = SKNode()
  var enemiesArray: [BadGuy] = []
//  var enemy: [Any]
  //Foreground
  var circles: [(circleWall: SKShapeNode, circleBackground: SKShapeNode)] = []
  var portals: [SKNode] = []
  var powerUps: [SKShapeNode] = []
  var maze: Maze
  /*
   The following variables define the HUD elements such as buttons, labels and simple shapes
   Each node has its own parent and every of them has a common anchestor
   Common anchestor is useful in case we need to move them all around or make them disappear all at once, saving lines of code...
   
   Important: Variables ordered by hierarchical order: parents -> children!
   Children are usually displayed on top of the parent (see zposition)
   
   */
  //Background is parent of every HUD element in the scene - Scene is background's only parent
  var background: SceneBackground
  var lifeSystem: LifeSystem
  var topRightHUD = SKNode()
  var pauseButton: PauseButton = PauseButton()
  var muteButton: MuteButton = MuteButton()
  var middleLayer: MiddleLayer
  var gameResult: GameResult = GameResult()
  var nextAction: NextAction = NextAction()
//  var homeButton: HomeButton = HomeButton()
  
  var countTouches = Int(0)
  var screenOrientationState: String!
  var viewController : UIViewController!

  var homeView = MainViewController()
  var storyBoard = UIStoryboard(name: "Main", bundle: nil)
  
  let countdownLabel = SKLabelNode()
  //Audio
  let backgroundSound = (UIApplication.shared.delegate as! AppDelegate).soundTrack
  // used to control the background music
  var soundState: Constants.SoundState = UserDefaults.standard.bool(forKey: Constants.MusicState.MUSIC_MUTED) ? .isAudioMuted : .isAudioPlaying
  
  // MARK: - Variables that define the base level
  
  // used to manage the varoious states of the game
  var gameState: Constants.GameState = .isPlaying
  

  var inGameNumberOfLives = 0
  
  //variable responsible for the movement of the badGuy
  var timer = Timer()
  
  // MARK: - Initialization of the base variables and the class
  override init(size: CGSize) {
    self.player = Hero(circleOfRadius: GlobalVariable.SCREEN_UNIT * levels[indexLevel].player.sizeMultiplier, positionOfHero: levels[indexLevel].player.position)
    self.lifeSystem = LifeSystem(inGameNumberOfLives: 1, heartOperand: 0, lifeSystemType: 2)
    self.background = SceneBackground(sceneBackgroundSize: size)
    self.middleLayer = MiddleLayer(layerSize: size)
    self.homeView = storyBoard.instantiateViewController(withIdentifier: "MainStoryboard") as! MainViewController
    GlobalVariable.SCREEN_UNIT = 0.03 * size.width
    self.maze = Maze(circleRadius: size.width, numberOfCircles: levels[indexLevel].numberOfCircles, heroSizeMultiplier: levels[indexLevel].player.sizeMultiplier)
    super.init(size: size)
    
    
    anchorPoint = CGPoint(x: 0.5, y: 0.5)
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  //deinit method is called on instances of classes before they are destroyed
  deinit {
    print("Remove NotificationCenter Deinit")
    NotificationCenter.default.removeObserver(self)
    timer.invalidate()

  }
  
  override func sceneDidLoad() { //First function called in the scene
    print(">.sceneDidLoad")
    
    HapticFeedback.notification.generator.prepare()
    
    NotificationCenter.default.addObserver(self, selector: #selector(getScreenOrientation), name: UIDevice.orientationDidChangeNotification, object: nil)
    
    NotificationCenter.default.addObserver(self,
                                           selector: #selector(self.applicationDidTimeout(notification:)),
                                           name: .appTimeout,
                                           object: nil
    )

    
  }
  

  
  // MARK: - Creation of the scene
  override func didMove(to view: SKView) {
    print(">.DidMove")

    setGlobalVariables()
    //Create Game User Interface
    setSceneBackground()
    //Set main path/maze
    setCircles()
    //Position as parameter so that we can clone the Hero passing a different position for future levels
    setHero()
    //Set Hero enemies
    setEnemies()
    //PowerUps give the Hero skills and powers
    setPowerUp()

    createHUD()
  }
  
  func setGlobalVariables() {
    print(">.setGlobalVariables")
    
    /*
     This method is called in order to first set some important variables such as the physics of the game scene and units used to improve multi-device adaptability
     */
    
    //Set a phisical body for the frame (SKView)
    //Other phisical bodies will not go beyond the scene
    physicsBody = SKPhysicsBody(edgeLoopFrom: frame)
    
    //Variable defined by the screen width (changes upon different devices) -- [ADAPTIVE]
//    GlobalVariable.SCREEN_UNIT = 0.03 * size.width
    
    //Set the scene as a physic protocol delegate
    physicsWorld.contactDelegate = self
    
    //Set gravity of the scene. To zero
    physicsWorld.gravity = .zero

    print("GlobalVariable.SCREEN_UNIT: \(GlobalVariable.SCREEN_UNIT)")
    
    print("UIScreen.main.bounds.width: \(UIScreen.main.bounds.width)")
    print("size.width: \(size.width)")
    //Load life for the character
    inGameNumberOfLives = levels[indexLevel].player.numberOfLives
    print("Hero starts the game with \(inGameNumberOfLives)")
    
  }
  
  // MARK: - Creation of HUD

  func createHUD() {
    print(">.createHUD")
    
    //    #if DEBUG
    //    //Set insets - Unsafe areas : CGRect
    //    setViewInsets()
    //    #endif
    
    setTopHUD()
    setBottomHUD()
    
  }
  
  
  func setSceneBackground() {
    print(">.setSceneBackground")
    
    /*
     This method sets the background of the scene thru its properties, then adds it to the scene
     */
    
    addChild(background)
  }
  
  func setTopHUD() {
    print(">.setTopHUD")
    
    setTopLeftHUD()
    setTopCenterHUD()
    setTopRightHUD()
  }
  
  func setTopLeftHUD() {
    print(">.setTopLeftHUD")
    
    //Set the life System
    lifeSystem = LifeSystem(inGameNumberOfLives: inGameNumberOfLives, heartOperand: 0, lifeSystemType:  2)
    
    inGameNumberOfLives = lifeSystem.setLifeHearts(inGameNumberOfLives: inGameNumberOfLives, heartOperand: 0)
    addChild(lifeSystem)
    lifeSystem.position = CGPoint(x: frame.minX + LEFTSAFEAREA + lifeSystem.calculateAccumulatedFrame().width/2 + 15, y:  frame.maxY - TOPSAFEAREA - lifeSystem.calculateAccumulatedFrame().height - 20)
    print("zpositionHea: \(lifeSystem.zPosition)")
  }
  
  func setTopCenterHUD() {
    print(">.setTopCenterHUD")
    
    
    /*
     This method is designated to set the Label that states: "Game Over" or "You win"
     */
    
    addChild(gameResult)
    gameResult.position.y = frame.maxY - TOPSAFEAREA - 100
    
    //    addChild(homeButton)
    //    homeButton.position = CGPoint(x: frame.midX , y: frame.midY - homeButton.calculateAccumulatedFrame().height + 15)

  }
  
  func setTopRightHUD() {
    print(">.setTopRightHUD")
    
    //Set Pause and Mute buttons/nodes
    //set pause button properties
    topRightHUD.addChild(pauseButton)
    pauseButton.position = CGPoint(x: 0, y: 0)
    addChild(middleLayer)
    
    
    //set mute button properties
    topRightHUD.addChild(muteButton)
    muteButton.position = CGPoint(x: pauseButton.position.x, y: pauseButton.position.y - pauseButton.calculateAccumulatedFrame().height - 10 )
    
    
    //Now the elements above are children of topRightHUD node, making easy to move all of them together
    addChild(topRightHUD)
    topRightHUD.name = "topRightHUD"
    
    topRightHUD.position = CGPoint(x: frame.maxX - RIGHTSAFEAREA - 20 - pauseButton.calculateAccumulatedFrame().width/2, y: frame.maxY - TOPSAFEAREA - 10 - pauseButton.calculateAccumulatedFrame().height/2)
    
  }
  
  func setBottomHUD() {
    print(">.setBottomHUD")
    
    /*
     This method sets the button that states: "Try again" or "Next Level"
     */
    
    addChild(nextAction)
    nextAction.position = CGPoint(x: frame.midX, y: frame.minY + BOTTOMSAFEAREA + 80)
    
    
  }
  
  
  func setViewInsets() {
    print(">.setViewInsets")
    
    
    /*
     This method is completely made for testing purposes:
     The call to this method will affect the game interface since we add nodes to the scene
     Display unsafe areas/insets will be colored in red
     Notch area will be colored in yellow
     Blue represents the game area: SAFE AREA
     There's the possibility to use this method to optimize the game HUD
     */
    
    //Rectangles declaration
    let topUnsafeAreaRect = CGRect(x: frame.minX, y: frame.maxY - TOPSAFEAREA, width: frame.width, height: TOPSAFEAREA)
    let bottomUnsafeAreaRect = CGRect(x: frame.minX, y: frame.minY, width: frame.width, height: BOTTOMSAFEAREA)
    let leftUnsafeAreaRect = CGRect(x: frame.minX, y: frame.minY, width: LEFTSAFEAREA, height: frame.height)
    let rightUnsafeAreaRect = CGRect(x: frame.maxX - RIGHTSAFEAREA, y: frame.minY, width: RIGHTSAFEAREA, height: frame.height)
    let notchUnsafeAreaRect = CGRect(x: frame.midX - frame.width/4 - 10, y: frame.maxY - TOPSAFEAREA + 12, width: frame.width/2 + 20, height: TOPSAFEAREA - 12)
    let gameAreaRect = CGRect(x: frame.minX + LEFTSAFEAREA, y: frame.minY + BOTTOMSAFEAREA, width: frame.width - RIGHTSAFEAREA - LEFTSAFEAREA, height: frame.height - TOPSAFEAREA - BOTTOMSAFEAREA)
    
    //Nodes declaration
    let topUnsafeAreaNode = SKShapeNode(rect: topUnsafeAreaRect)
    let bottomUnsafeAreaNode = SKShapeNode(rect: bottomUnsafeAreaRect)
    let leftUnsafeAreaNode = SKShapeNode(rect: leftUnsafeAreaRect)
    let rightUnsafeAreaNode = SKShapeNode(rect: rightUnsafeAreaRect)
    let notchUnsafeAreaNode = SKShapeNode(rect: notchUnsafeAreaRect)
    let gameAreaNode = SKShapeNode(rect: gameAreaRect)
    
    //Define colors
    gameAreaNode.fillColor = .blue
    topUnsafeAreaNode.fillColor = .red
    bottomUnsafeAreaNode.fillColor = .red
    leftUnsafeAreaNode.fillColor = .red
    rightUnsafeAreaNode.fillColor = .red
    notchUnsafeAreaNode.fillColor = .yellow
    
    //Add nodes to the scene
    addChild(topUnsafeAreaNode)
    addChild(bottomUnsafeAreaNode)
    addChild(leftUnsafeAreaNode)
    addChild(rightUnsafeAreaNode)
    addChild(notchUnsafeAreaNode)
    addChild(gameAreaNode)
    
  }
  
  // MARK: - Creation of nodes
  
  func setHero() {
    print("setHero")
    
    player = Hero(circleOfRadius: GlobalVariable.SCREEN_UNIT * levels[indexLevel].player.sizeMultiplier, positionOfHero: levels[indexLevel].player.position)
    
    player.hero.name = "player.hero"
    player.name = "player"
    addChild(player)
    
  }
  
  func setEnemies() {
  
    for enemy in levels[indexLevel].enemy {
      switch enemy.type {
      case EnemyType.badGuy :
        print("case BadGuy")
        setBadGuys(enemy: enemy)

      default:
        print("no enemies")
        return
      }
      
    }

    
  }
  
  func setBadGuys(enemy: LevelEnemy) {
    print(">.setBadGuys")
    
    timer = Timer.scheduledTimer(timeInterval: 1.00, target: self, selector: #selector(badGuysDisplacement), userInfo: nil, repeats: true)
    
    for i in 0..<levels[indexLevel].numberOfEnemies {
      print("generating badGuy \(i)")
      let enemy = BadGuy(circleOfRadius: GlobalVariable.SCREEN_UNIT * enemy.sizeMultiplier)
      enemy.badGuy.name = "badGuy\(i)"
      enemy.name = "BadGuy\(i)"
      
      print("badGuy.name = \(String(describing: enemy.badGuy.name))")
      setBadGuy(enemy: enemy)
      
    }
    
    addChild(enemies)
    //    print(Unmanaged.passUnretained(enemy).toOpaque())
    
    timer.fire()
  }
  
  func setBadGuy(enemy: BadGuy) {
    print(">.setBadGuy")
    
    let minRadius = (CGFloat(view!.frame.width * 2 * 0.097) + GlobalVariable.SCREEN_UNIT) / 2
    let maxRadius = (CGFloat(view!.frame.width * 0.97) - GlobalVariable.SCREEN_UNIT) / 2
    
    let startAngle = CGFloat(CGFloat.random(in: 0...(2 * .pi)))
    
    let randX = frame.midX + CGFloat.random(in: minRadius...maxRadius) * cos(startAngle)
    let randY = frame.midY + CGFloat.random(in: minRadius...maxRadius) * sin(startAngle)
    
    enemy.badGuy.position = CGPoint(x: randX, y: randY)
    enemiesArray.append(enemy)
    enemies.addChild(enemy)
    
  }
  
  
  //this funtion handles the movement of the purity balls
  @objc func badGuysDisplacement() {
    
    DispatchQueue.main.async {
      
      //You can use the array
      for child in self.enemiesArray {
        let randomX = CGFloat.random(in: -2.5..<2.5)
        let randomY = CGFloat.random(in: -2.5..<2.5)
        
        child.badGuy.run(SKAction.applyForce(CGVector(dx: randomX, dy: randomY), duration: 1), withKey: "badGuysDisplacement")
      }
      
      //You can also use the main node
      //      for i in 0..< self.enemies.children.count {
      //        if let grandson = self.enemies.childNode(withName: "BadGuy\(i)")?.childNode(withName: "badGuy\(i)") {
      ////          print("grandson.name = \(String(describing: grandson.name))")
      //          let randomX = CGFloat.random(in: -2.5..<2.5)
      //          let randomY = CGFloat.random(in: -2.5..<2.5)
      //          grandson.run(SKAction.applyForce(CGVector(dx: randomX, dy: randomY), duration: 1))
      //
      //        }
      //      }
    }
  }
  
  func setCircles() {
    print(">.setCircles")
    addChild(maze)
    
  }
    
  // MARK: - Actions
  
  // this function handles the contacts between the nodes
  func didBegin(_ contact: SKPhysicsContact) {
    //    print(">.didBegin")
    
    //When hero touches a badGuy
    if contact.bodyA.categoryBitMask == Constants.PhysicsCategory.heroBitMask, contact.bodyB.categoryBitMask == Constants.PhysicsCategory.badGuyBitMask, gameState == .isPlaying {
      contact.bodyB.node?.removeFromParent() // remove the purity ball
      print(contact.bodyB.hashValue)
      //Remove the badGuy that hero touched
      
      // sound (?)
      //Subtract a life from the max
      loseLife(numOfLivesLost: -1)
    }
    //When badGuy touched Hero
    if contact.bodyA.categoryBitMask == Constants.PhysicsCategory.badGuyBitMask, contact.bodyB.categoryBitMask == Constants.PhysicsCategory.heroBitMask, gameState == .isPlaying {
      contact.bodyA.node?.removeFromParent() // remove the purity ball
      print(contact.bodyA.hashValue)
      //Remove the badGuy that hero touched
      
      // sound (?)
      
      //Subtract a life from the max
      loseLife(numOfLivesLost: -1)
    }
    //When hero touches the background - Hero escapes
    if contact.bodyA.categoryBitMask == Constants.PhysicsCategory.heroBitMask, contact.bodyB.categoryBitMask == Constants.PhysicsCategory.sceneBackgroundBitMask, gameState == .isPlaying {
      //Check number of Lives remained
      if (inGameNumberOfLives >= 1){
        //You win
        winLevel()
      }
    }
    //When the background touches hero
    if contact.bodyA.categoryBitMask == Constants.PhysicsCategory.sceneBackgroundBitMask, contact.bodyB.categoryBitMask == Constants.PhysicsCategory.heroBitMask, gameState == .isPlaying {
      //Check number of Lives remained
      if (inGameNumberOfLives >= 1){
        //You win
        winLevel()
      }
    }
    //When a resizingPowerUp touches Hero
    if contact.bodyA.categoryBitMask == Constants.PhysicsCategory.resizingPowerUpBitMask, contact.bodyB.categoryBitMask == Constants.PhysicsCategory.heroBitMask {
      //Reduce the size of Hero
      reduceHeroSize()
    }
    //When hero touches a resizingPowerUp
    if contact.bodyA.categoryBitMask == Constants.PhysicsCategory.heroBitMask, contact.bodyB.categoryBitMask == Constants.PhysicsCategory.resizingPowerUpBitMask {
      //Reduce the size of Hero
      reduceHeroSize()
      
    }
    
  }
  
  override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
    let touch = touches.first
    let touchLocation = touch!.location(in: self)
    let pauseTouchLocation = touch!.location(in: self.pauseButton)
    let muteTouchLocation = touch!.location(in: self.muteButton)
    let nextActionTouchLocation = touch!.location(in: self.nextAction)
//    let homeTouchLocation = touch!.location(in: self.homeButton)
  
    // Check if the location of the touch is within the button's bounds
    //if tryAgainButton is pressed -> reload game scene
    if nextAction.nextAction.contains(nextActionTouchLocation) {
      print("nextAction.touched")
      if(gameState == .isPaused) {
        // with this istruction the game doesn't freezes if you press the "try again" button while the game is pause
        self.isPaused = false
//        self.view?.isPaused = false

      }
      
      gameStatusAction()
    }
    if pauseButton.pauseButton.contains(pauseTouchLocation) && (gameState == .isPlaying || gameState == .isPaused) {
      
      pauseButtonManager()
    }
    if muteButton.muteButton.contains(muteTouchLocation) && (gameState == .isPlaying || gameState == .isPaused) {
      muteButtonManager()
    }
    
//    if homeButton.homeContainer.contains(homeTouchLocation) && gameState == .isPaused {
//      homeButton.buttonPressed()
//      self.viewController.present(homeView, animated: true, completion: nil)
//    }
    
    #if DEBUG
    //    *********** THE FOLLOWING IS TO BE COMMENTED *************
    //    ******************* ONLY FOR DEBUGGERS *******************
    if player.hero.contains(touchLocation) && gameState == .isPlaying {
      countTouches += 1
      print("Hero touched \(countTouches) times")
      if countTouches == 10{
        debugCheats(sender: "player.hero")
        countTouches = 0
      }
      HapticFeedback.notification.generator.notificationOccurred(.error)

    }
    
    
    if lifeSystem.contains(touchLocation) && gameState == .isPlaying {
      print("heart touched and lost: life\(inGameNumberOfLives - 1)")
      
      debugCheats(sender: "heartSprite")
      
    }
    
    
    
    #endif
    //    **********************************************************
  }
  
  // this function manages the mute button
  func muteButtonManager() {
    print(">.muteButtonManager")
    
    if soundState == .isAudioPlaying {
      soundState = .isAudioMuted
      muteButton.switchIcon()
      backgroundSound?.stop()
      UserDefaults.standard.set(true, forKey: Constants.MusicState.MUSIC_MUTED)
    }
    else if soundState == .isAudioMuted {
      soundState = .isAudioPlaying
      muteButton.switchIcon()
      backgroundSound?.currentTime = 0
      backgroundSound?.play()
      UserDefaults.standard.set(false, forKey: Constants.MusicState.MUSIC_MUTED)
    }
  }
  
  // this function manages the pause button
  func pauseButtonManager() {
    print(">.pauseButtonManager")
    
    if gameState == .isPlaying {
      gameState = .isPaused
      pauseButton.pauseResume(pause: true)
      middleLayer.showPauseLayer(isHidden: false)
      nextAction.alpha = 1
//      homeButton.alpha = 1
      player.stopMotion()
      timer.invalidate()
      UIApplication.shared.isIdleTimerDisabled = false
      
      DispatchQueue.main.asyncAfter(deadline: .now() + 0.01) {
        self.isPaused = true
//        self.view?.isPaused = true

      }
      
      
    } else if gameState == .isPaused {
      gameState = .isPlaying
      nextAction.alpha = 0
//      homeButton.alpha = 0
      
      player.setMotionManager()
      pauseButton.pauseResume(pause: false)
      middleLayer.showPauseLayer(isHidden: true)
      
      timer = Timer.scheduledTimer(timeInterval: 1.00, target: self, selector: #selector(badGuysDisplacement), userInfo: nil, repeats: true)
      timer.fire()

      DispatchQueue.main.asyncAfter(deadline: .now() + 0.01) {
        //You can pause the view instead (nothing will be able to animate while on pause):
//        self.view?.isPaused = false
        self.isPaused = false
      }
      
      

      
    }
  }
  
  func gameStatusAction() {
    print(">.gameStatusAction")
    
    //Define the game states
    switch gameState {
    //Level is over
    case .isFinished(finalScore: _, remainingLives: _):
      nextLevel()
    case .isLost:
      replayLevel()
    case .isPaused:
      replayLevel()
    default:
      print(".isPlaying")
      return
    }
  }
  
  func nextLevel() {
    print("nextLevel")
    
    //Checks if there is a next existing level - if not, restarts the last level
    if indexLevel < levels.count - 1 {
      indexLevel += 1
    } else {
      replayLevel()
    }
    
    if indexLevel > 0 {
      UserDefaults.standard.set(false, forKey: GlobalVariable.FIRST_LAUNCH)

    }
    
    
    print("indexLevel: \(indexLevel)")
    let gameScene = GameScene(size: self.size)
    // create type of transition (you can check in documentation for more transtions)
    let transition = SKTransition.fade(withDuration: 1.0)
    self.view?.presentScene(gameScene, transition: transition)
  }
  
  func replayLevel() {
    print("replayLevel")
    // create your new scene
    
    let gameScene = GameScene(size: self.size)
    // create type of transition (you can check in documentation for more transtions)
    let transition = SKTransition.fade(withDuration: 1.0)
    self.view?.presentScene(gameScene, transition: transition)
  }
  
  func loseLife(numOfLivesLost: Int) {
    print(">.loseLife")
    
    //Decrease number of lives available to the player
    print("Lives before losing one: \(inGameNumberOfLives)")
    print("Lives remaining: \(inGameNumberOfLives + numOfLivesLost)")
    
    //Plays vibration on devices (except iPad, doesn't have it)
    //    AudioServicesPlayAlertSound(kSystemSoundID_Vibrate)
    
    player.heroGetsDarker()
    
    if inGameNumberOfLives + numOfLivesLost <= 0 {
      print("Hearts and Lives: 0")
      print("inGameNumberOfLives + numberOfLivesLost = \(inGameNumberOfLives) + \(numOfLivesLost)")
      loseLevel()
      return
    } else {
      
      inGameNumberOfLives = lifeSystem.setLifeHearts(inGameNumberOfLives: inGameNumberOfLives, heartOperand: numOfLivesLost)
      
      
    }
    
    
  }
  
  func addLife(add numOfLivesGained: Int) {
    print(">.addLife: add \(numOfLivesGained) lives")
    player.heroGetsLighter()
    //    setLifeHearts(heartIncrementer: numOfLivesGained)
    inGameNumberOfLives = lifeSystem.setLifeHearts(inGameNumberOfLives: inGameNumberOfLives, heartOperand: numOfLivesGained)
    
  }
  
  
  //No lives left, Game Over
  func loseLevel() {
    print(">.loseLevel")
    gameState = .isLost
    
    player.stopMotion()
    //    motionManager?.stopAccelerometerUpdates()
//    timer.invalidate()
    
    Constants.Animation.playAppearingSequence1(node: nextAction)

    hideHUD()
    
    //Runs the sequence of animations
    player.playDeathSequence1()
    
    //Stops the hero from moving around
    player.hero.physicsBody?.isDynamic = false
    
  }
  
  
  
  func winLevel() {
    print(">.winLevel")
    
    gameState = .isFinished(finalScore: 0, remainingLives: inGameNumberOfLives)
    // this is simply to avoid calling this function over and over again
    player.hero.physicsBody?.contactTestBitMask = 0
    timer.invalidate()
    
    HapticFeedback.notification.success
    
    print("Level Won")
//    finalGameStatusLbl.text = Constants.GameScene.YOU_WON
    gameResult.switchText()
    nextAction.switchForWin()
    
    //        writeJSON()
    
    hideHUD()
    removeAllBadGuys()
    
  }
  
  func hideHUD() {
    print(">.hideHUD")
    
    Constants.Animation.playAppearingSequence1(node: gameResult)

    Constants.Animation.playAppearingSequence1(node: nextAction)

    Constants.Animation.playHidingSequence1(node: lifeSystem, destroy: true)
    
    Constants.Animation.playHidingSequence1(node: topRightHUD, destroy: false)
    
    
    
  }
  
  //Remove all the badguys from the game
  func removeAllBadGuys() {
    
    //remove all badGuys using their common array structure
    for i in 0..<enemiesArray.count {
      enemiesArray[i].playDeathSequence1()
      print("enemy\(i) dies")
      
    }
    DispatchQueue.main.asyncAfter(deadline: .now() + 4.0, execute: {
      
      //remove all enemies using their common parent node
      self.enemies.removeAllChildren()
      self.enemies.removeFromParent()
      print("All enemies removed!")
    })
    
  }
  
  //
  func setPowerUp() {
    print(">.setPowerUp")
    for i in 0..<levels[indexLevel].powerUp.count {
      
      switch levels[indexLevel].powerUp[i].type {
      case .resizing:
        print("case resizing")
        setResizingPowerUp(name: i)
        
      default:
        break
      }
      
    }
    
  }
  
  //
  func setResizingPowerUp(name: Int) {
    print(">.setResizingPowerUp")
    let minRadius = (CGFloat(view!.frame.width * 2 * 0.097) + GlobalVariable.SCREEN_UNIT) / 2
    let maxRadius = (CGFloat(view!.frame.width * 0.97) - GlobalVariable.SCREEN_UNIT) / 2
    
    let startAngle = CGFloat(CGFloat.random(in: 0...(2 * .pi)))
    
    let randX = frame.midX + CGFloat.random(in: minRadius...maxRadius) * cos(startAngle)
    let randY = frame.midY + CGFloat.random(in: minRadius...maxRadius) * sin(startAngle)
    
    let powerUp = SKShapeNode(rectOf: CGSize(width: 1.5 * GlobalVariable.SCREEN_UNIT, height: 1.5 * GlobalVariable.SCREEN_UNIT), cornerRadius: 0 * GlobalVariable.SCREEN_UNIT)
    powerUp.strokeColor = #colorLiteral(red: 0.8807368875, green: 0.8755016923, blue: 0.8847613931, alpha: 1)
    powerUp.fillColor = #colorLiteral(red: 0.8807368875, green: 0.8755016923, blue: 0.8847613931, alpha: 1)
    powerUp.zPosition = Constants.Layer.PowerUp
    powerUp.position = CGPoint(x: randX, y: randY)
    powerUp.physicsBody = SKPhysicsBody(rectangleOf: CGSize(width: 1.5 * GlobalVariable.SCREEN_UNIT, height: 1.5 * GlobalVariable.SCREEN_UNIT))
    powerUp.physicsBody?.categoryBitMask = Constants.PhysicsCategory.resizingPowerUpBitMask
    powerUp.physicsBody?.contactTestBitMask = Constants.PhysicsCategory.heroBitMask
    powerUp.physicsBody?.collisionBitMask = Constants.PhysicsCategory.portalBitMask | Constants.PhysicsCategory.badGuyBitMask | Constants.PhysicsCategory.resizingPowerUpBitMask
    
    powerUp.name = "resizingPowerUp\(name)"
    
    powerUps.append(powerUp)
    addChild(powerUp)
    
    
  }
  
  //Reduce size of the Hero
  func reduceHeroSize(){
    print(">.reduceHeroSize")
    
    
    player.playReduceSize1()
    //    countdown(count: 10)
    
    removeAllPowerUps()
  }
  
  //TO DO: Still not implemented - Increase size of the hero
  func increaseHeroSize(){
    print(">.increaseHeroSize")
    
    
    player.playIncreaseSize1()
    
    countdown(count: 10)
    
    
    removeAllPowerUps()
    
  }
  
  //Remove all power ups from the scene
  func removeAllPowerUps() {
    print(">.removeAllPowerUps")
    
    //remove all powerUps
    powerUps.forEach { (powerUp) in
      powerUp.removeFromParent()
    }
  }
  
  func countdown(count: Int) {
    print(">.countdown")
    
    var counts : Int
    counts = count
    countdownLabel.position = CGPoint(x: 0, y: 0)
    countdownLabel.fontColor = SKColor.white
    countdownLabel.fontSize = size.height / 30
    countdownLabel.color = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
    countdownLabel.zPosition = 99
    countdownLabel.text = "Launching hero in \(count)..."
    
    addChild(countdownLabel)
    
    let counterDecrement = SKAction.sequence([SKAction.wait(forDuration: 1.0),
                                              SKAction.run{
                                                counts = counts - 1
                                                self.countdownLabel.text = "Launching hero in \(counts)..."
                                                
      }])
    
    run(SKAction.sequence([SKAction.repeat(counterDecrement, count: 10),
                           SKAction.run(endCountdown)]))
    
  }
  
  func endCountdown() {
    print(">.endCountdown")
    
    countdownLabel.removeFromParent()
    player.hero.physicsBody!.applyImpulse(CGVector(dx: 20, dy: 20))
  }
  
  func debugCheats(sender: String) {
    print(">.debugCheats")
    
    print("debug.sender: \(sender)")
    
    if sender == "player.hero" {
      //      winLevel()
      addLife(add: 1)
    }
    
    if sender == "heartSprite" {
      //Subtract a life from the max
      loseLife(numOfLivesLost: -1)
    }
    
  }
  
  @objc func getScreenOrientation() {
    print(">.getScreenOrientation")
    
    var rad : CGFloat = 0
    
    switch UIDevice.current.orientation {
    case .portrait:
      screenOrientationState = "Portrait"
      
      
        Constants.Animation.rotate(node: pauseButton, rad: rad)
        Constants.Animation.rotate(node: muteButton, rad: rad)
      Constants.Animation.rotate(node: nextAction, rad: rad)
      Constants.Animation.rotate(node: lifeSystem, rad: rad)

      lifeSystem.position = CGPoint(x: frame.minX + LEFTSAFEAREA + lifeSystem.calculateAccumulatedFrame().width/2 + 15, y:  frame.maxY - TOPSAFEAREA - lifeSystem.calculateAccumulatedFrame().height - 20)

        moveMuteButton()

    case .portraitUpsideDown:
      screenOrientationState = "PortraitUpsideDown"
      rad = .pi
      

        Constants.Animation.rotate(node: pauseButton, rad: rad)
        Constants.Animation.rotate(node: muteButton, rad: rad)
      Constants.Animation.rotate(node: nextAction, rad: rad)
      Constants.Animation.rotate(node: lifeSystem, rad: rad)

      lifeSystem.position = CGPoint(x: frame.minX + LEFTSAFEAREA + lifeSystem.calculateAccumulatedFrame().width/2 + 15, y:  frame.maxY - TOPSAFEAREA - lifeSystem.calculateAccumulatedFrame().height - 20)

        moveMuteButton()

    case .landscapeLeft:
      screenOrientationState = "LandscapeLeft"
      rad = -.pi/2
      

        Constants.Animation.rotate(node: pauseButton, rad: rad)
        Constants.Animation.rotate(node: muteButton, rad: rad)
      Constants.Animation.rotate(node: nextAction, rad: rad)
      Constants.Animation.rotate(node: lifeSystem, rad: rad)

      lifeSystem.position = CGPoint(x: frame.minX + LEFTSAFEAREA + lifeSystem.calculateAccumulatedFrame().width/2 + 15, y:  frame.maxY - TOPSAFEAREA - lifeSystem.calculateAccumulatedFrame().height - 20)

        moveMuteButton()


    case .landscapeRight:
      screenOrientationState = "LandscapeRight"
      rad = .pi/2
      

        Constants.Animation.rotate(node: pauseButton, rad: rad)
        Constants.Animation.rotate(node: muteButton, rad: rad)
        Constants.Animation.rotate(node: nextAction, rad: rad)
      Constants.Animation.rotate(node: lifeSystem, rad: rad)

      lifeSystem.position = CGPoint(x: frame.minX + LEFTSAFEAREA + lifeSystem.calculateAccumulatedFrame().width/2 + 15, y:  frame.maxY - TOPSAFEAREA - lifeSystem.calculateAccumulatedFrame().height - 20)

        moveMuteButton()

    default:
      screenOrientationState = "Another"
    }
    
    print("Device in '\(screenOrientationState!)' mode" )
    
    
  }
  
  func moveMuteButton() {
    muteButton.position = CGPoint(x: pauseButton.position.x, y: pauseButton.position.y - pauseButton.calculateAccumulatedFrame().height - 10 )

  }
  

  @objc func applicationDidTimeout(notification: NSNotification) {
    
    print("application did timeout, perform actions")
    //show a message, maybe the user is stucked on something (help, UX)
    
    pauseButtonManager()
    
  }

  
  //  func writeJSON() {
  //    print("writeJSON")
  //
  //    self.saveSessions[saveSessionIndex].id = saveSessionIndex + 1
  //    self.saveSessions[saveSessionIndex].levelIdentifier = indexLevel
  //
  //    do{
  //      try GenericCoder.encodeInFile(withName: .SaveSessions, withCollection: self.saveSessions) {
  //        print("SAVESESSIONS: \(saveSessions)")
  //      }
  //    } catch let error {
  //      print("loadJSON: \(error)")
  //    }
  //  }
  
}



