//
//  PauseButton.swift
//  WashTheWater
//
//  Created by Luigi Barretta on 03/01/2019.
//  Copyright © 2019 AcademyProjects. All rights reserved.
//

import Foundation
import SpriteKit

class PauseButton: SKNode {

  //Button for pausing the game
  var pauseButton: SKShapeNode!
  let pauseIcon = SKSpriteNode(imageNamed: Constants.ImageName.PlayButton)
  let resumeIcon = SKSpriteNode(imageNamed: Constants.ImageName.PauseButton)
  
  
  override init() {
    super.init()
    setup()
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  func setup() {
    print(">.setPauseResumeButton")
        
    let adaptiveHeight = GlobalVariable.SCREEN_UNIT * 2.5
    
    pauseButton = SKShapeNode(rect: CGRect(origin: .zero, size: CGSize(width: adaptiveHeight * 1.764, height: adaptiveHeight)), cornerRadius: adaptiveHeight * 0.358)
    
    //
    pauseIcon.size = CGSize(width: pauseButton.frame.size.width/2.5, height: pauseButton.frame.size.width/2)
    resumeIcon.size = CGSize(width: pauseButton.frame.size.width/2.5, height: pauseButton.frame.size.width/2)
    //anchorPoint x: 0 goes right, y: 0 goes up. 0.5 0.5 are default values
    pauseIcon.anchorPoint = CGPoint(x: 0.4, y: 0.55)
    resumeIcon.anchorPoint = CGPoint(x: 0.55, y: 0.55)
    
    pauseButton.position = CGPoint(x: 0 - pauseButton.frame.width/2, y: 0 - pauseButton.frame.height/2)
    
    pauseIcon.position = CGPoint(x: pauseButton.frame.width / 2, y: pauseButton.frame.height / 2)
    resumeIcon.position = CGPoint(x: pauseButton.frame.width / 2, y: pauseButton.frame.height / 2)
    
    pauseButton.fillColor = .white
    pauseButton.lineWidth = 5
    pauseButton.strokeColor = .white
    
    resumeIcon.isHidden = true
    //Set pause Label properties
    pauseButton.addChild(pauseIcon)
    pauseButton.addChild(resumeIcon)
    //        pauseResumeContainer.addChild(pauseShadow)


    self.addChild(pauseButton)
    self.zPosition = Constants.Layer.ForegroundOfHUD

    
  }
  
  func pauseResume(pause: Bool) {
    print(">.pauseResume")
    
    pauseIcon.isHidden = pause
    resumeIcon.isHidden = !pause
    HapticFeedback.selection.selectionChanged()
  }
  
  func rotate(rad: CGFloat) {
//    self.run(SKAction.sequence([SKAction.fadeAlpha(to: 0, duration: 0),SKAction.wait(forDuration: 0.1), SKAction.customAction(withDuration: 0, actionBlock: { (node, elapsedTime) in self.zRotation = rad}),SKAction.fadeAlpha(to: 1, duration: 0)]))
    self.zRotation = rad

  }

  
  
  
}
