//
//  MuteButton.swift
//  WashTheWater
//
//  Created by Luigi Barretta on 03/01/2019.
//  Copyright © 2019 AcademyProjects. All rights reserved.
//

import Foundation
import SpriteKit

class MuteButton: SKNode {
  
  var muteButton: SKShapeNode!
  let muteIcon = SKSpriteNode(imageNamed: Constants.ImageName.SoundOFF)
  let unmuteIcon = SKSpriteNode(imageNamed: Constants.ImageName.SoundON)

  
  override init() {
    super.init()
    setup()
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  func setup() {
    
    print(">.setMuteButton")

    let adaptiveHeight = GlobalVariable.SCREEN_UNIT * 2.5
    let isMuted = UserDefaults.standard.bool(forKey: Constants.MusicState.MUSIC_MUTED)

    muteButton = SKShapeNode(rect: CGRect(origin: .zero, size: CGSize(width: adaptiveHeight * 1.764, height: adaptiveHeight)), cornerRadius: adaptiveHeight * 0.358)
    
    muteButton.fillColor = .white
    muteButton.lineWidth = 5
    muteButton.strokeColor = .white
    
    muteIcon.size = CGSize(width: muteButton.frame.size.width/2.5, height: muteButton.frame.size.width/2.5)
    unmuteIcon.size = CGSize(width: muteButton.frame.size.width/2.5, height: muteButton.frame.size.width/2.5)
    //anchorPoint x: 0 goes right, y: 0 goes up. 0.5 0.5 are default values
    muteIcon.anchorPoint = CGPoint(x: 0.5, y: 0.5)
    unmuteIcon.anchorPoint = muteIcon.anchorPoint
    
    muteButton.position = CGPoint(x: 0 - muteButton.frame.width/2, y: 0 - muteButton.frame.height/2)
    
    muteIcon.position = CGPoint(x: muteButton.frame.width / 2, y: muteButton.frame.height / 2)
    unmuteIcon.position = CGPoint(x: muteButton.frame.width / 2, y: muteButton.frame.height / 2)
    
    muteIcon.isHidden = !isMuted
    unmuteIcon.isHidden = isMuted
    //Set pause Label properties
    muteButton.addChild(muteIcon)
    muteButton.addChild(unmuteIcon)
    self.zPosition = Constants.Layer.ForegroundOfHUD
    self.addChild(muteButton)

    
  }
  
  func switchIcon() {
    
    
    if muteIcon.isHidden == false && unmuteIcon.isHidden == true {
      muteIcon.isHidden = true
      unmuteIcon.isHidden = false

    } else {
      muteIcon.isHidden = false
      unmuteIcon.isHidden = true

    }

    HapticFeedback.selection.selectionChanged()


    
  }
  
  
}
