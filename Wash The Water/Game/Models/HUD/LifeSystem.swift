//
//  LifeSystem.swift
//  WashTheWater
//
//  Created by Luigi Barretta on 03/01/2019.
//  Copyright © 2019 AcademyProjects. All rights reserved.
//

import Foundation
import SpriteKit

class LifeSystem: SKNode {

  //HeartSystem -> HeartsContainer -> HeartsNode -> HeartSprite

  private let inGameNumberOfLives : Int
  private let heartOperand : Int
  private let heartSystem : SKNode = SKNode()
  private var heartsContainer : SKShapeNode = SKShapeNode()
  public let heartsNode : SKNode = SKNode()
  private var heartSprite: SKSpriteNode! = SKSpriteNode()
  private let lifeSystemType: Int


  init(inGameNumberOfLives: Int, heartOperand: Int, lifeSystemType: Int) {
    self.inGameNumberOfLives = inGameNumberOfLives
    self.heartOperand = heartOperand
    self.lifeSystemType = lifeSystemType
    
    super.init()

    setup()
  }

  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

  func setLifeHearts(inGameNumberOfLives: Int, heartOperand: Int) -> Int {
    print(">.setLifeHearts")
    /*
     Given a number of lives (JSON file), show an equal amount of sprites
     The following code defines three main GUI components:
     - Hearts: a spriteNode, image in media.xcassets
     - HeartsNode: a node that contains all the hearts
     - Container: contains the heartsNode
     Do not exclude: this algorithm can be optimized in the future
     */

    var inGameNumberOfLives = inGameNumberOfLives

    //Constants that will make table-like GUI for displaying hearts (lives)
    var maxRow = 2 // >0 !
    var maxCol = 3 // >0 !
    //Initialization
    var numRow = 0
    var numCol = 0

    print("inGameNumberOfLives: \(inGameNumberOfLives)")
    print("heartOperand: \(heartOperand)")

    if heartOperand < 0 && lifeSystemType == 2 {
    
      for i in (inGameNumberOfLives + heartOperand)...(inGameNumberOfLives - 1) {
        if let child = self.heartsNode.childNode(withName: "\(Constants.NodesName.heart)\(i)") as? SKSpriteNode {
          playHidingSequence2(childSprite: child)

          print("Heart \(String(describing: child.name)) fades out")
          //        Replaces last displayed heart image with another one "BrokenHeart"
          //        child.texture = SKTexture(imageNamed: "HalfHeart")
          
          //With the following you can remove heartSprite from the parent HeartsNode
          //        child.removeFromParent()
        }
      }
      
    }
    
    //JSON-Derived Formal error management - Lives can't have negative values
    if inGameNumberOfLives < 0 {
      inGameNumberOfLives = (inGameNumberOfLives * -1)
      print("inGameNumberOfLives < 0, new: \(inGameNumberOfLives)")

    } else if inGameNumberOfLives == 0 {
      inGameNumberOfLives = 1
    }

    //Max number of columns becomes the number indicated in the JSON when this is lower than maxCol
    if inGameNumberOfLives < maxCol {
      maxCol = inGameNumberOfLives + 1
      maxRow = 1
      print("Hearts Container resized to max: \(maxCol) on x")
    }

    //Maximum number of displayable hearts: grid of hearts + container
    //Important: Max hearts per row don't have to be over 9, otherwise it looks ugly!
    //Advise: 4x2
    let maxNumberOfHearts = maxRow * maxCol
    print("Max displayable hearts: \(maxRow) x \(maxCol) = \(maxNumberOfHearts)")


    //Check on JSON, if lives number is greater than the max displayable amount of lives
    //Set var used to control number of lives (decrease or increase)
    if  (inGameNumberOfLives + heartOperand > maxNumberOfHearts) {
      inGameNumberOfLives = maxNumberOfHearts
      print("inGame lives can't exceed the maxNumberOfHearts, set to: \(inGameNumberOfLives)")
    } else {
      if heartOperand != 0 {
        inGameNumberOfLives += heartOperand

        print("Life incremented or decremented by \(heartOperand): \(inGameNumberOfLives)")
      }

    }

    //Initialize the main node of which all the hearts will be appended children
    print("heartsNode.NumberOfSprites: \(heartsNode.children.count)")
    print("heartsNode.removeAllChildren")
    //Remove both children and parent - Parent is the node that contains all the sprites
    //Removing only the parent won't remove the children from the scene!
    heartsNode.removeAllChildren()
    heartsNode.removeFromParent()
    heartSystem.removeAllChildren()
    heartSystem.removeFromParent()

    self.removeAllChildren()

    print("heartsNode.NumberOfSprites: \(heartsNode.children.count)")

    //Debug - Print all heartsNode children - Should be empty
    for child in heartsNode.children {
      print("heartsNode.children: \(String(describing: child.name))")
    }

    //This first loop manages the displayable rows (y)
    repeat {
      //      print("Hearts Rows Loop Enters: \(numRow)")
      //This second loop manages the displayable columns (x) -> sprite
      while (Int(numCol) < maxCol) && (heartsNode.children.count < inGameNumberOfLives) {
        
        let colSpacing : CGFloat
        let rowSpacing : CGFloat
        
        //        print("Hearts Cols Loop Enters: \(numCol)")
        //Create new instance of livesLeft at every loop - avoid error
        heartSprite = SKSpriteNode(imageNamed: Constants.ImageName.Life)

        //Define custom size of heart sprite: 26x21 (11.25 is GlobalVariable.SCREEN_UNIT value for iPhone XR)
        heartSprite.size = CGSize(width: (26 * GlobalVariable.SCREEN_UNIT)/11.25, height: (21 * GlobalVariable.SCREEN_UNIT)/11.25)
        //Define position of each heart based on the row and column
        
        colSpacing = 0
        rowSpacing = 0
        
        heartSprite.position = CGPoint(x: 0 + (heartSprite.size.width + colSpacing) * (1 + CGFloat(numCol)), y: 0 - (heartSprite.size.height + rowSpacing) * (1 + CGFloat(numRow)))

        //Gives id to each sprite
        heartSprite.name = "\(Constants.NodesName.heart)\(heartsNode.children.count)"
        //Hearts have a common parent - heroLives
        heartsNode.addChild(heartSprite)
        heartsNode.position = CGPoint(x: 0, y: 0)
        print("❤️", terminator:"")
        //Column increments
        numCol += 1
      }
      //      print("Hearts Cols Loop Exits: \(numCol)")
      print("\n")
      //Initialize columns since we need a heart right at the beginning of the container (col, row)
      numCol = 0
      //Row increments
      numRow += 1
      //The loop exits when the needed n. of rows is reached and the n. of hearts (starts from 0) is equal to the in-game lives
    } while(Int(numRow) < maxRow) && (heartsNode.children.count < inGameNumberOfLives)
    print("Hearts Rows Loop Exits: \(numRow)")
    
    //Debug
    for child in heartsNode.children {
      print("Heart child: \(String(describing: child.name))")
    }

    //Add container first to the main heartSystem Node
    setHeartsContainer()

    print("add Hearts to scene")
    print("heartsNode n children: \(heartsNode.children.count)")

    //Assign identifier to the direct parent of the heartSprites
    heartsNode.name = "heartsNode"
    //Add node to common parent heartSystem
    heartsNode.zPosition = Constants.Layer.BackgroundOfHUD
    heartSystem.addChild(heartsNode)
    print("initial number of hearts: \(heartsNode.children.count)")
    print("initial number of lives: \(inGameNumberOfLives)")

    //HeartSystem -> HeartsContainer -> HeartsNode -> HeartSprite
    heartSystem.name = "heartSystem"
    heartSystem.position = CGPoint(x: 0 - heartsContainer.frame.midX, y: 0 - heartsContainer.frame.midY)
    self.addChild(heartSystem)
    
    if inGameNumberOfLives == 1 {
      playPulseSequence()
    }

    self.zPosition = Constants.Layer.BackgroundOfHUD
    
    return inGameNumberOfLives
  }
    


  func setHeartsContainer() {
    //Dynamic resizing of the container based on the number of hearts
    //When the number of hearts is greater than the max amount of displayable hearts on x axis
    //Then give the container the max length and appropriate heigth

    
    let space: CGFloat = 15
    
    print(">.setHeartsContainer")
    heartsContainer.removeAllChildren()
    heartsContainer.removeFromParent()
    print("heartsContainer removed")

    for child in heartsContainer.children {
      print("heartsContainer.children: \(child.name ?? "nil")")
    }
    
    print("heartsNode.children.count: \(heartsNode.children.count)")
      print("heartsContainer reshaped 1")
      

    heartsContainer = SKShapeNode(rect: CGRect(x: heartsNode.calculateAccumulatedFrame().minX - space/2, y: heartsNode.calculateAccumulatedFrame().minY - space/2 , width: heartsNode.calculateAccumulatedFrame().width + space, height: heartsNode.calculateAccumulatedFrame().height + space), cornerRadius: 20)

    //Define properties of the container
//    heartsContainer.lineWidth = 15
    heartsContainer.strokeColor = .white
    heartsContainer.fillColor = .white
    heartsContainer.zPosition = Constants.Layer.HUDContainer
    
    heartSystem.addChild(heartsContainer)
    
  }
  
  func setup() {
    
    self.name = "LifeSystem"
  }
  
  func playPulseSequence() {
    //Pulse animation: heart beats - Last heart will pulse
    let sequence = SKAction.repeatForever(SKAction.sequence([Constants.Animation.scaleOut1,SKAction.customAction(withDuration: 0, actionBlock: {
      (node, elapsedTime) in
      HapticFeedback.impact.heavy.impactOccurred()
      
    }),SKAction.wait(forDuration: 0.2),Constants.Animation.scaleNormalize1]))
    
    if let child = heartsNode.childNode(withName: "\(Constants.NodesName.heart)0") as? SKSpriteNode {
      print("Heart pulses...")
      child.run(sequence, withKey: "HeartPulse")

    }


  }
  
  
  func playHidingSequence2(childSprite: SKSpriteNode) {
    childSprite.run(Constants.Animation.fadeOut1, withKey: "HidingSequence2")
  }

}
