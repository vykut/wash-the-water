//
//  HomeButton.swift
//  WashTheWater
//
//  Created by Luigi Barretta on 05/01/2019.
//  Copyright © 2019 AcademyProjects. All rights reserved.
//

import Foundation
import SpriteKit

class HomeButton: SKNode {
  
  public var homeButton: SKShapeNode!
  private let homeIcon = SKSpriteNode(imageNamed: Constants.ImageName.HomeWhite)
  private let homeLbl = SKLabelNode(text: "Home")

  override init() {
    super.init()
    setup()
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  func setup() {
    
    print(">.setMuteButton")
    
    let homeContainerHeight = GlobalVariable.SCREEN_UNIT * 2.5
    
    homeButton = SKShapeNode(rect: CGRect(origin: .zero, size: CGSize(width: homeContainerHeight * 2 + homeLbl.frame.width, height: homeContainerHeight + homeLbl.frame.height)), cornerRadius: homeContainerHeight * 0.358)
    
    homeButton.fillColor = Constants.Palette.IMAGE
    homeButton.lineWidth = 5
    homeButton.strokeColor = .white
    
    homeIcon.size = CGSize(width: homeButton.frame.size.width/7, height: homeButton.frame.size.width/7)
    
    homeButton.position = CGPoint(x: 0 - homeButton.frame.width/2, y: 0 - homeButton.frame.height/2)
    
    //Set pause Label properties
    homeButton.zPosition = Constants.Layer.BackgroundOfHUD
    homeLbl.color = .white
    homeButton.addChild(homeIcon)
    homeButton.addChild(homeLbl)
    homeLbl.position = CGPoint(x: 0 + homeButton.frame.width/1.75 , y: 0 + homeButton.frame.height/4 )
    homeIcon.position = CGPoint(x: homeButton.frame.width/6, y: homeLbl.frame.midY)


    self.addChild(homeButton)
    
    self.alpha = 0
    self.name = "HomeButton"

    
  }
  
  func buttonPressed() {
    
    HapticFeedback.selection.selectionChanged()
    SKAction.resize(toWidth: homeButton.frame.width/1.5, height: homeButton.frame.height/1.5, duration: 0)
    
    
  }
    
  
  
}
