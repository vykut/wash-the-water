//
//  BlackLayer.swift
//  WashTheWater
//
//  Created by Luigi Barretta on 04/01/2019.
//  Copyright © 2019 AcademyProjects. All rights reserved.
//

import Foundation
import SpriteKit

class MiddleLayer: SKNode {
  
  //Button for pausing the game
  private let middleLayer = SKSpriteNode()
  private let layerSize : CGSize
  
  init(layerSize: CGSize) {
    self.layerSize = layerSize
    super.init()
    setup()
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  func setup() {
    print(">.setPauseResumeButton")
        
    middleLayer.isHidden = true
    middleLayer.size = layerSize
    middleLayer.color = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.5)
    middleLayer.zPosition = Constants.Layer.PauseLayer
    
    self.addChild(middleLayer)
    
    middleLayer.anchorPoint = CGPoint(x: 0.5, y: 0.5)
    
  }
  
  func showPauseLayer(isHidden: Bool) {
    middleLayer.isHidden = isHidden
  }
  
}

