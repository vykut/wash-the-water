//
//  GameResult.swift
//  WashTheWater
//
//  Created by Luigi Barretta on 04/01/2019.
//  Copyright © 2019 AcademyProjects. All rights reserved.
//

import Foundation
import SpriteKit

class GameResult: SKNode {
  
  private let gameResult = SKLabelNode(text: Constants.GameScene.GAME_OVER)
  
  override init() {
    
    super.init()
    
    setup()
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  func setup() {
    
    gameResult.fontSize = 55 //Originally 75
    gameResult.fontColor = UIColor(hex: 0x1E1E1E)
    gameResult.position = CGPoint(x: 0, y: 0 - gameResult.frame.height/2)
    gameResult.zPosition = Constants.Layer.BackgroundOfHUD
    self.addChild(gameResult)
    self.alpha = 0
    self.name = "GameResult"
  }
  
  func switchText() {
    
    switch gameResult.text {
    case Constants.GameScene.YOU_WON:
      gameResult.text = Constants.GameScene.GAME_OVER
    default:
      gameResult.text = Constants.GameScene.YOU_WON

    }
    
    gameResult.text = Constants.GameScene.YOU_WON


  }
  

  
}
