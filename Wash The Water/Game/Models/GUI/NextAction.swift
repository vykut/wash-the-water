//
//  NextAction.swift
//  WashTheWater
//
//  Created by Luigi Barretta on 05/01/2019.
//  Copyright © 2019 AcademyProjects. All rights reserved.
//

import Foundation
import SpriteKit

class NextAction: SKNode {
  
  public var nextAction = SKShapeNode()
  private let nextActionLbl = SKLabelNode(text: Constants.GameScene.TRY_AGAIN)
  private let nextActionIcon = SKSpriteNode(imageNamed: Constants.ImageName.TryAgain)

  override init() {
    
    super.init()
    
    setup()
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  func setup() {
    
    nextAction = SKShapeNode(rect: CGRect(origin: .zero, size: CGSize(width: 100, height: 60)), cornerRadius: 10) //150, 60, 10
    nextAction.position = CGPoint(x: 0 - nextAction.frame.width/2, y: 0 - nextAction.frame.height/2);
    nextAction.fillColor = .white
    nextAction.lineWidth = 0
    nextAction.zPosition = Constants.Layer.ForegroundOfHUD
    
    //Label
    nextActionLbl.position = CGPoint(x: nextAction.frame.midX, y: nextAction.frame.midY - 10)
    nextActionLbl.fontColor = UIColor(hex: 0x1E1E1E)
    nextActionLbl.isHidden = true
    //Icon
    nextActionIcon.size = CGSize(width: (UIImage(named: Constants.ImageName.TryAgain)?.size.width ?? 40)/2, height: (UIImage(named: Constants.ImageName.TryAgain)?.size.height ?? 50)/2)
    nextActionIcon.position = CGPoint(x: nextAction.frame.width / 2, y: nextAction.frame.height / 2)
    
    nextAction.addChild(nextActionIcon)
    
    //Create parent-child relationship
    nextAction.addChild(nextActionLbl)
    //Add to scene
    self.addChild(nextAction)
    
    self.alpha = 0
    self.name = "NextAction"

  }
      
  func switchForWin() {
    nextActionLbl.text = Constants.GameScene.NEXT_LEVEL
    nextActionIcon.texture = SKTexture(imageNamed: Constants.ImageName.NextLevel)
    nextActionIcon.size = CGSize(width: (UIImage(named: Constants.ImageName.NextLevel)?.size.width ?? 50)/2, height: (UIImage(named: Constants.ImageName.NextLevel)?.size.height ?? 50)/2)

  }

  
  
}










