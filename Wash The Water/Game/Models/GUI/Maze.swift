//
//  Maze.swift
//  WashTheWater
//
//  Created by Luigi Barretta on 06/01/2019.
//  Copyright © 2019 AcademyProjects. All rights reserved.
//

import Foundation
import SpriteKit

class Maze: SKNode {
  
  private let maze : SKNode = SKNode()
  private let circleRadius : CGFloat
  private let numberOfCircles : Int
  private let heroSizeMultiplier : CGFloat
  
  init(circleRadius: CGFloat, numberOfCircles: Int, heroSizeMultiplier: CGFloat) {
    self.circleRadius = circleRadius
    self.numberOfCircles = numberOfCircles
    self.heroSizeMultiplier = heroSizeMultiplier
    
    super.init()
    
    setup()
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  func setup() {
    
    var i = numberOfCircles - 1
    
    repeat {
      let circles = SKNode()
      let portals = SKNode()
      
      circles.name = "circles"
      portals.name = "portals"
      
      var circleBorder: SKShapeNode
      let radius = CGFloat(circleRadius * CGFloat(i + 1) * 0.097)
      
      //Assigning a random position for the portal
      let startAngle = CGFloat.random(in: 0...(2 * .pi))
      let endAngle = CGFloat(startAngle + 2 * .pi - (GlobalVariable.SCREEN_UNIT * heroSizeMultiplier * 2.5 / radius))
      
      //Calls func createPortal and returns a portal
      let portal = createPortal(startAngle: startAngle, endAngle: endAngle, radius: radius+3)
      portal.name = "portal\(i)"
      //Define the path of current circleBorder having a different radius at each loop
      let circlePath = UIBezierPath(arcCenter: .zero, radius: radius, startAngle: startAngle, endAngle: endAngle, clockwise: true)
      //Assign a radius to circleBackground
      let circleBackground = SKShapeNode(circleOfRadius: radius)
      circleBackground.name = "circleBackground\(i)"
      //Point (x,y) of origin further assigned to the circles
      
      //UI properties of the circles
      circleBorder = SKShapeNode(path: circlePath.cgPath)
      circleBorder.lineWidth = 0
      circleBorder.strokeColor = Constants.Palette.CIRCLES_BACKGROUND[i]
      circleBorder.zPosition = Constants.Layer.CircleBorder
      circleBorder.name = "circleBorder\(i)"
      
      //Physical properties of the circles
      circleBorder.physicsBody = SKPhysicsBody(edgeChainFrom: circlePath.cgPath)
      circleBorder.physicsBody?.affectedByGravity = false
      circleBorder.physicsBody?.allowsRotation = false
      //Set properties of circles
      circleBackground.fillColor = Constants.Palette.CIRCLES_BACKGROUND[i]
      circleBackground.lineWidth = 0
      circleBackground.strokeColor = Constants.Palette.CIRCLES_BACKGROUND[i].darker(by: 20)
      circleBackground.zPosition = Constants.Layer.Circles
      
      portals.addChild(circleBorder)
      circles.addChild(circleBackground)
      portals.addChild(portal)
      
      maze.addChild(circles)
      maze.addChild(portals)
      
      //Animates the first and second portals created at different rotation velocity
      if i == numberOfCircles - 1 {
        print("ANIMATE")
        let rotation = SKAction.rotate(byAngle: 1.5 * .pi, duration: 5)
        let action = SKAction.repeatForever(rotation)
        portals.run(action, withKey: "action")
      } else if i == numberOfCircles - 2 {
        let rotation = SKAction.rotate(byAngle: -1 * .pi, duration: 5)
        let action = SKAction.repeatForever(rotation)
        portals.run(action, withKey: "action")
      }
      //Iterator
      i -= 1
    } while (i >= 0)
    
    self.addChild(maze)
    maze.name = "maze"
    self.name = "MAZE"
    
  }
  
  func createPortal(startAngle: CGFloat, endAngle: CGFloat, radius: CGFloat) -> SKNode {
    print(">.createPortal")
    //Assign a path to the portal
    let portalPath = UIBezierPath(arcCenter: .zero, radius: radius, startAngle: endAngle, endAngle: startAngle, clockwise: true)
    
    let portal = SKShapeNode(path: portalPath.cgPath)
    portal.lineWidth = 5
    portal.strokeColor = .white
    portal.zPosition = Constants.Layer.Portal
    portal.physicsBody = SKPhysicsBody(edgeLoopFrom: portalPath.cgPath)
    portal.physicsBody?.categoryBitMask = Constants.PhysicsCategory.portalBitMask
    portal.physicsBody?.collisionBitMask = Constants.PhysicsCategory.badGuyBitMask
    portal.physicsBody?.contactTestBitMask =  Constants.PhysicsCategory.badGuyBitMask
    
    return portal
  }
  
  
  func animate(node: SKNode) {
    
    let rotation = SKAction.rotate(byAngle: 1.5 * .pi, duration: 5)
    let action = SKAction.repeatForever(rotation)
    node.run(action, withKey: "action")
    
  }
  
  
  
}

