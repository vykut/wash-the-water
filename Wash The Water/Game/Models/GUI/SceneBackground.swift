//
//  Background.swift
//  WashTheWater
//
//  Created by Luigi Barretta on 03/01/2019.
//  Copyright © 2019 AcademyProjects. All rights reserved.
//

import Foundation
import SpriteKit

class SceneBackground: SKNode {
  
  private let sceneBackground: SKSpriteNode = SKSpriteNode()
  private let sceneBackgroundSize: CGSize


  init(sceneBackgroundSize: CGSize) {
    self.sceneBackgroundSize = sceneBackgroundSize

    super.init()
    
    setup()
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  func setup() {

    sceneBackground.color = Constants.Palette.BACKGROUND
    sceneBackground.size = sceneBackgroundSize
    sceneBackground.zPosition = Constants.Layer.Background
    //Set physical body for the background -- Player wins when touches it (need it for physical contact)
    sceneBackground.physicsBody = SKPhysicsBody(edgeLoopFrom: sceneBackground.frame)
    sceneBackground.physicsBody?.categoryBitMask = Constants.PhysicsCategory.sceneBackgroundBitMask
    sceneBackground.physicsBody?.collisionBitMask = Constants.PhysicsCategory.sceneBackgroundBitMask
    //Contact managed
    sceneBackground.physicsBody?.contactTestBitMask = Constants.PhysicsCategory.heroBitMask
    sceneBackground.anchorPoint = CGPoint(x: 0.5, y: 0.5)
    self.addChild(sceneBackground)
  }
  
}
