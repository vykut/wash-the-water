//
//  BadGuy.swift
//  WashTheWater
//
//  Created by Luigi Barretta on 30/12/2018.
//  Copyright © 2018 AcademyProjects. All rights reserved.
//

import Foundation


import SpriteKit

class BadGuy: SKNode {
  var defaultAction: Constants.Animation = Constants.Animation()
  
  // MARK: Variables
  public let badGuy: SKShapeNode
  private let circleOfRadius: CGFloat

  // MARK: Init
  
  init(circleOfRadius: CGFloat) {
    self.circleOfRadius = circleOfRadius
    
    self.badGuy = SKShapeNode(circleOfRadius: circleOfRadius)
    
    self.badGuy.name = "DefaultbadGuyName"
  
    super.init()
    
    self.name = "DefaultbadGuySKNode"

    setup()
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  // MARK: - Setup
  func setup() {
    
      self.badGuy.strokeColor = UIColor.black
      self.badGuy.fillColor = UIColor.black
      self.badGuy.zPosition = Constants.Layer.BadGuy
//      self.badGuy.position = CGPoint(x: randX, y: randY)
      self.badGuy.physicsBody = SKPhysicsBody(circleOfRadius: self.circleOfRadius)
      self.badGuy.physicsBody?.categoryBitMask = Constants.PhysicsCategory.badGuyBitMask
      self.badGuy.physicsBody?.contactTestBitMask = Constants.PhysicsCategory.heroBitMask | Constants.PhysicsCategory.portalBitMask
      self.badGuy.physicsBody?.collisionBitMask = Constants.PhysicsCategory.portalBitMask | Constants.PhysicsCategory.badGuyBitMask


    self.addChild(badGuy)
    
  }
  
  func playDeathSequence1() {
    let sequence = SKAction.sequence([Constants.Animation.scaleIn1, Constants.Animation.scaleNormalize1, Constants.Animation.fadeOut1])
    self.badGuy.run(sequence, withKey: "BadGuyDeath1")
    
  }
  
  
}
