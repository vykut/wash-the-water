//
//  Hero.swift
//  WashTheWater
//
//  Created by Luigi Barretta on 29/12/2018.
//  Copyright © 2018 AcademyProjects. All rights reserved.
//

import Foundation
import SpriteKit
import CoreMotion

class Hero: SKNode {
  
  // MARK: Variables
  public let hero: SKShapeNode
  private let circleOfRadius: CGFloat
  private let positionOfHero: CGPoint
  private let motionManager: CMMotionManager = CMMotionManager()

  // MARK: Init
  
  init(circleOfRadius: CGFloat, positionOfHero: CGPoint) {
    self.circleOfRadius = circleOfRadius
    self.positionOfHero = positionOfHero
    
    hero = SKShapeNode(circleOfRadius: circleOfRadius)
    
    super.init()
    
    self.name = "DefaultHeroSKNode"
    self.hero.name = "DefaultHeroName"
    
    setup()
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  // MARK: - Setup
  func setup() {
    
    hero.strokeColor = .white
    hero.fillColor = .white
    hero.glowWidth = 2

    hero.zPosition = Constants.Layer.Hero
    hero.position = positionOfHero
    hero.physicsBody = SKPhysicsBody(circleOfRadius: circleOfRadius) 
    
    hero.physicsBody?.categoryBitMask = Constants.PhysicsCategory.heroBitMask
    hero.physicsBody?.collisionBitMask =  Constants.PhysicsCategory.badGuyBitMask
    hero.physicsBody?.contactTestBitMask = Constants.PhysicsCategory.badGuyBitMask | Constants.PhysicsCategory.sceneBackgroundBitMask

    //Add hero to Hero (SKNode)
    self.addChild(hero)
    
    //Wait for one second so that the player doesn't spawn in the wrong position before the scene loads
    DispatchQueue.main.asyncAfter(deadline: .now() + 1.0, execute: {
      
      //Activate its motion (accelerometer)
      self.setMotionManager()
    })
  }
  
  func setMotionManager() {

    print("setMotionManager")
    motionManager.accelerometerUpdateInterval = 1 / 120 // originally 80 fps
    //print(motionManager.accelerometerUpdateInterval)
    let gameSpeedMultiplier: Double = frame.width <= 800 ? 20 : 500 //Originally set by Vic at 800? 40 : 500
    print("gameSpeedMultiplier: \(gameSpeedMultiplier)")
    print("frame.width: \(self.frame.width)")
    motionManager.startAccelerometerUpdates(to: OperationQueue()) { (data, error) in
      guard let data = data else { print(error!.localizedDescription); return }
      //print(data.acceleration)
      
      //Speed of the hero can be imported from the JSON file as well
      //      self.hero.physicsBody?.applyForce(CGVector(dx: data.acceleration.x * Double(levels[indexLevel].gameSpeedMultiplier), dy: data.acceleration.y * Double(levels[indexLevel].gameSpeedMultiplier)))
      
      
      //Original force applied to the Hero
      self.hero.physicsBody?.applyForce(CGVector(dx: data.acceleration.x * gameSpeedMultiplier, dy: data.acceleration.y * gameSpeedMultiplier))
      
      
    }
  }
  
  func stopMotion() {
    //Stop hero Motion - Encapsulation
    self.motionManager.stopAccelerometerUpdates()

  }
  
  func playDeathSequence1() {
    let sequence = SKAction.sequence([Constants.Animation.scaleIn1, Constants.Animation.scaleNormalize1, Constants.Animation.fadeOut1])
    self.hero.run(sequence, withKey: "HeroDeath1")
    HapticFeedback.notification.error
  }
  
  func playReduceSize1() {
    let sequence = SKAction.sequence([Constants.Animation.scaleIn2, SKAction.wait(forDuration: 10), Constants.Animation.scaleNormalize2])
    self.hero.run(sequence, withKey: "HeroReduceSize1")

  }
  
  func playIncreaseSize1() {
    let sequence = SKAction.sequence([Constants.Animation.scaleOut2, SKAction.wait(forDuration: 10), Constants.Animation.scaleNormalize2])
    self.hero.run(sequence, withKey: "HeroIncreaseSize1")
    
  }
  
  func heroGetsDarker() {
    //Used to darken Hero - 1 = no changes
    let darkenFactor: CGFloat = 1.3

    let darkenedHero = UIColor(displayP3Red: (self.hero.fillColor.cgColor.components![0] / darkenFactor), green: (self.hero.fillColor.cgColor.components![1] / darkenFactor), blue: (self.hero.fillColor.cgColor.components![2] / darkenFactor), alpha: 1)
    
    HapticFeedback.impact.heavy.impactOccurred()

    self.hero.fillColor = darkenedHero

  }

  func heroGetsLighter() {
    //Used to darken Hero - 1 = no changes
    let lightenFactor: CGFloat = 1.3
    
    let lightenedHero = UIColor(displayP3Red: (self.hero.fillColor.cgColor.components![0] * lightenFactor), green: (self.hero.fillColor.cgColor.components![1] * lightenFactor), blue: (self.hero.fillColor.cgColor.components![2] * lightenFactor), alpha: 1)
    
    self.hero.fillColor = lightenedHero
    
    
    
  }



}
