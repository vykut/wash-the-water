//
//  GameViewController.swift
//  Wash The Water
//
//  Created by Victor Socaciu on 11/12/2018.
//  Copyright © 2018 AcademyProjects. All rights reserved.
//

import UIKit
import SpriteKit

var levels: [Level] = []

class GameViewController: UIViewController {
  
var gameArea = CGRect()

  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    loadJSON()
    guard let view = view as? SKView else { return }
    let scene = GameScene(size: view.bounds.size)
    
    scene.viewController = self
    /* Sprite Kit applies additional optimizations to improve rendering performance */
    view.ignoresSiblingOrder = false
    
    #if DEBUG
      //Shows fps and node count at the bottom right of the screen
      // places borders around any physicsBody, This outline on the objects is the physicsBody that we attached to them
//      view.showsPhysics = true
    view.showsFPS = true
    view.showsDrawCount = true
    view.showsNodeCount = true
    #endif
    
    view.presentScene(scene)
    
    // Do any additional setup after loading the view.
  }
  
  override func viewWillAppear(_ animated: Bool) {
    //This hides the navigation controller on the top border
    navigationController?.setNavigationBarHidden(true, animated: false)
  }
  
  override var shouldAutorotate: Bool {
    return true
  }
  
  
  
  override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
    if UIDevice.current.userInterfaceIdiom == .phone {
      return .allButUpsideDown
    } else {
      return .all
    }
  }
  
  override var prefersStatusBarHidden: Bool {
    return true
  }
  
  func loadJSON() {
    print("loadJSON")
    do{
      if let loadLevels: [Level] = try GenericCoder.decodeFromFile(withName: FileNames.Levels) {
        levels = loadLevels
        print("LEVELS: \(levels)")
      }
    } catch let error {
      print("loadJSON: \(error)")
    }
    
    print("Number of Levels loaded: \(levels.count)")
  }
  


}
