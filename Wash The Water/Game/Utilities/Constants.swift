//
//  Constants.swift
//  WashTheWater
//
//  Created by Victor Socaciu on 16/12/2018.
//  Copyright © 2018 AcademyProjects. All rights reserved.
//

import Foundation
import UIKit
import SpriteKit

struct Constants {
  struct GameScene {
    static let GAME_SCENE = "GameScene"
    static let TRY_AGAIN = "Try Again"
    static let GAME_OVER = "Game Over!"
    static let PAUSE = "Pause"
    static let YOU_WON = "You Win!"
    static let RESUME = "Resume"
    static let PLAY_AGAIN = "Play again!"
    static let NEXT_LEVEL = "Next Level!"
  }
  
  struct MusicState {
    static let MUSIC_MUTED = "MusicMuted"
    static let MUTE = "Mute"
    static let UNMUTE = "Unmute"
  }
  
  struct Palette {
    static let BACKGROUND = UIColor(hex: 0xDAF2FE)
    static let CIRCLES_BACKGROUND: [UIColor] = [.init(hex: 0x075B6), .init(hex: 0x48BEFF), .init(hex: 0x6DCBFF), .init(hex: 0x91D8FE), .init(hex: 0xB6E5FF)]
    static let IMAGE = UIColor(hex: 0x35BED0)
  }
  
  struct ImageName {
    static let PlayButton = "PlayButton"
    static let PauseButton = "PauseButton"
    static let SoundOFF = "SoundOFF"
    static let SoundON = "SoundON"
    static let TryAgain = "TryAgain"
    static let NextLevel = "NextLevel"
    static let CrocMouthOpen = "CrocMouthOpen"
    static let CrocMask = "CrocMask"
    static let Prize = "Pineapple"
    static let PrizeMask = "PineappleMask"
    static let Life = "Life"
    static let Home = "Home"
    static let HomeWhite = "HomeWhite"
    
    
  }
  
  struct SoundFile {
    static let BackgroundMusic = "washTheWaterSoundtrack"
    static let OldBackgroundMusic = "riverFlowsInYou"
  }
  
  struct Layer {
    static let Background: CGFloat = 0
    static let Circles: CGFloat = 10
    static let Portal: CGFloat = 20
    static let CircleBorder: CGFloat = 20
    static let PowerUp: CGFloat = 30
    static let Hero: CGFloat = 40
    static let BadGuy: CGFloat = 50
    static let HUDContainer: CGFloat = 70
    static let BackgroundOfHUD: CGFloat = 80
    static let PauseLayer: CGFloat = 90
    static let ForegroundOfHUD: CGFloat = 100
  }
  
  // Declaration of the bitmasks - These are used to distinguish the physical object
  struct PhysicsCategory {
    static let badGuyBitMask : UInt32 = 0x1 << 1
    static let heroBitMask : UInt32 = 0x1 << 2
    static let sceneBackgroundBitMask : UInt32 = 0x1 << 3
    static let portalBitMask : UInt32 = 0x1 << 4
    static let resizingPowerUpBitMask : UInt32 = 0x1 << 5
    
  }
  
  struct Animation {
    
    // MARK: Variables
    static let fadeOut1 = SKAction.fadeOut(withDuration: 1)
    
    static let scaleIn1 = SKAction.scale(to: 0.7, duration: 0.5)
    static let scaleIn2 = SKAction.scale(to: 0.5, duration: 2)
    
    static let scaleOut1 = SKAction.scale(to: 1.3, duration: 0.5)
    static let scaleOut2 = SKAction.scale(to: 1.5, duration: 2)
    static let scaleOut3 = SKAction.scale(to: 1.25, duration: 0.5)
    
    static let scaleNormalize1 = SKAction.scale(to: 1, duration: 0.5)
    static let scaleNormalize2 = SKAction.scale(to: 1, duration: 2)
    
    static let resize1 = SKAction.resize(toWidth: 0.5, duration: 0.5)
    
    static let fadeIn1 = SKAction.fadeIn(withDuration: 5)
    
    static let fadeIn2 = SKAction.fadeAlpha(to: 1, duration: 1.5)
    
    static let custom1 = SKAction.sequence([SKAction.fadeAlpha(to: 0, duration: 0),SKAction.wait(forDuration: 0.1), SKAction.customAction(withDuration: 0, actionBlock: {
      (node, elapsedTime) in
      let node = SKSpriteNode()
      node.zRotation = -CGFloat(Double.pi/2)
      
    }),SKAction.fadeAlpha(to: 1, duration: 0)])
    
    static func playAppearingSequence1(node: SKNode) {
      let key = "\(node.name ?? "default")_AppearingSequence1"
      node.run(Constants.Animation.fadeIn2, withKey: key)
      print(key)
      
    }
    
    static func playHidingSequence1(node: SKNode, destroy: Bool) {
      let key = "\(node.name ?? "default")_HidingSequence1_\(destroy)"
      node.run(SKAction.sequence(
        [Constants.Animation.fadeOut1,
         SKAction.customAction(withDuration: 0, actionBlock: {
          (node, elapsedTime) in
          if destroy { node.removeFromParent() }
          
          
         })
        ]), withKey: key)
      print(key)
      
    }
    
    static func rotate(node: SKNode, rad: CGFloat) {
      let key = "\(node.name ?? "default")_Rotated"
      
      //    self.run(SKAction.sequence([SKAction.fadeAlpha(to: 0, duration: 0),SKAction.wait(forDuration: 0.1), SKAction.customAction(withDuration: 0, actionBlock: { (node, elapsedTime) in self.zRotation = rad}),SKAction.fadeAlpha(to: 1, duration: 0)]))
      node.zRotation = rad
      print(key)
      
      
    }
    
  }
  
  enum NodesName: String {
    case heart
  }
  
  enum SoundState: Equatable {
    case isAudioPlaying
    case isAudioMuted
  }
  
  enum GameState: Equatable {
    case isPlaying
    case isPaused
    case isLost
    case isFinished(finalScore: Int, remainingLives: Int)
  }
  
}







