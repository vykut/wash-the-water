//
//  Global.swift
//  WashTheWater
//
//  Created by Luigi Barretta on 08/01/2019.
//  Copyright © 2019 AcademyProjects. All rights reserved.
//

import Foundation
import CoreGraphics

struct GlobalVariable {
  static var SCREEN_UNIT = CGFloat(0.03)
  static var INACTIVITY_COUNTER = CGFloat(5)
  static var FIRST_LAUNCH = "FirstLaunch"
  static var GENERIC_SWITCH = false
}

