//
//  Constants.swift
//  WashTheWater
//
//  Created by Luigi Barretta on 04/01/2019.
//  Copyright © 2019 AcademyProjects. All rights reserved.
//

import Foundation
import SpriteKit
import AudioToolbox


struct HapticFeedback {
  
  static let selection = UISelectionFeedbackGenerator()
  struct notification {
    static let generator = UINotificationFeedbackGenerator()
    static let warning : Void = UINotificationFeedbackGenerator().notificationOccurred(.warning)
    static let success : Void = UINotificationFeedbackGenerator().notificationOccurred(.success)
    static let error : Void = UINotificationFeedbackGenerator().notificationOccurred(.error)
  }
  struct impact {
    static let generator = UIImpactFeedbackGenerator()
    static let heavy = UIImpactFeedbackGenerator(style: .heavy)
    static let medium = UIImpactFeedbackGenerator(style: .medium)
    static let light = UIImpactFeedbackGenerator(style: .light)
  }
  struct oldWay {
    /*
     http://iphonedevwiki.net/index.php/AudioServices
     https://github.com/TUNER88/iOSSystemSoundsLibrary
     */
      static let peek : Void = AudioServicesPlaySystemSound(1519) // Actuate `Peek` feedback (weak boom)
      static let pop : Void = AudioServicesPlaySystemSound(1520) // Actuate `Pop` feedback (strong boom)
      static let nope : Void = AudioServicesPlaySystemSound(1521) // Actuate `Nope` feedback (series of three weak booms)
      static let alert : Void = AudioServicesPlayAlertSound(kSystemSoundID_Vibrate)
  
  }
  

}
