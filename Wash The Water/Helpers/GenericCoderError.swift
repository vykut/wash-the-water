//
//  GenericCoderError.swift
//  WashTheWater
//
//  Created by Luigi Barretta on 20/12/2018.
//  Copyright © 2018 AcademyProjects. All rights reserved.
//

import Foundation

enum GenericCoderError: Error {
  case invalidURL
  case invalidDataCreation
  case invalidDecode
  case invalidEncode
  case invalidStringCreation
  case invalidWritingToFile
}
